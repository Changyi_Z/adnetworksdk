# Ad Network SDK @Wandoujia #

豌豆荚广告SDK，此版为可定制布局专版，开发者可以根据自己的展现要求，定制化广告位的样式。


### 接入方式 ###

* 引用library工程，注意添加libs文件夹下的jar包到buildpath（如果需要的话）
* 在主工程中添加权限（参照sample）

```
#!xml

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.READ_PHONE_STATE"/>
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
    <uses-permission android:name="com.android.launcher.permission.INSTALL_SHORTCUT" />
```
* 在主工程中添加声明（参照sample）

```
#!xml
<!--For AdsSdk.-->
        <activity
                android:name="com.wandoujia.ads.sdk.AppWallActivity"
                android:launchMode="singleTop"
                android:configChanges="orientation|keyboardHidden|screenSize"/>

        <activity
                android:name="com.wandoujia.ads.sdk.AppWidgetContainerActivity"
                android:theme="@android:style/Theme.Translucent.NoTitleBar"
                android:configChanges="orientation|keyboardHidden|screenSize"/>

        <service android:name="com.wandoujia.ads.sdk.DownloadService"/>

        <receiver android:name="com.wandoujia.ads.sdk.AppChangedReceiver">
            <intent-filter>
                <action android:name="android.intent.action.PACKAGE_ADDED"/>

                <data android:scheme="package"/>
            </intent-filter>
        </receiver>

```


### 调用方式 ###

1. 继承 SmartAdView，通过重写方法定制UI，如果不重写则采用默认样式。
```
#!java

static class MyCustomSmartAdView extends SmartAdView {

    public MyCustomSmartAdView(Context context, String id) {
      super(context, id);
    }

    public MyCustomSmartAdView(Context context, String appId, String secretKey, String id) throws Exception {
      super(context, appId, secretKey, id);
    }

    @Override
    public int getLayoutId() {
      return R.layout.custom_smart_ad_view;
    }

    @Override
    public int getBigPictureViewId() {
      return R.id.app_widget_big_picture;
    }

    @Override
    public String getDownloadText() {
      return "点击安装";
    }

    @Override
    public String getDownloadingText() {
      return "下载进行时";
    }
  }
```

其中

```
#!java
  // 返回自定义的整体Layout id
  public int getLayoutId();
  // 返回自定义的图标ImageView id，注意次控件必须继承自NetworkImageView
  public int getIconImageViewid();
  // 返回自定义标题 id，注意次控件必须继承自TextView
  public int getTitleViewId();
  // 返回自定义副标题 id，注意次控件必须继承自TextView
  public int getSubTitleViewId();
  // 返回自定义广告描述区域 id，注意次控件必须继承自TextView
  public int getDescriptionViewId();
  // 返回自定义安装按钮 id，注意次控件必须继承自Button
  public int getInstallButtonId();
  // 返回自定义关闭按钮 id，注意次控件必须继承自View
  public int getCloseButtonId();
  // 返回自定义大图 id，注意次控件必须继承自NetworkImageView
  // 建议此区域设定为自适应显示，因为不是所有广告都有大图展示
  public int getBigPictureViewId();
  // 返回安装按钮可点击安装时的字样
  public String getDownloadText();
  // 返回下载过程中的字样
  public String getDownloadingText();
```

2. 调用方法初始化，显示广告。
```
#!java

try {
      smartAdView =
          new MyCustomSmartAdView(this, ADS_APP_ID, ADS_SECRET_KEY, TAG_INTERSTITIAL_WIDGET).load(
              new AdListener() {
                @Override
                public void onAdLoaded() {

                }
              }).into((android.view.ViewGroup) findViewById(R.id.wdj_smart_ad_container));
    } catch (Exception e) {
      e.printStackTrace();
    }
```
其中，

```
#!java

/**
   * 构造方法初始化
   * @param context
   * @param appId 
   * @param secretKey
   * @param id 广告位ID
   * @throws Exception
   */
  public SmartAdView (Context context, String appId, String secretKey, String id)

/**
   * 请求加载广告
   * @param adListener 可以在 onAdLoaded触发自己的逻辑，广告SDK会在加载完成后自动渲染广告
   * @return
   */
  public final SmartAdView load(AdListener adListener)

/**
   * 添加广告位到指定容器view
   * @param parentView
   * @return
   * @throws Exception
   */
  public final SmartAdView into(ViewGroup parentView)

/**
   * 主动展示广告
   * @return
   * @throws Exception
   */
  public final SmartAdView show()

/**
   * 展示下一条广告
   * @return
   */
  public final SmartAdView showNext()

/**
   * 设定关闭按钮点击事件，默认为关闭当前Activity
   * @param onClickListener
   * @return
   */
  public SmartAdView listenCloseButtonClick(View.OnClickListener onClickListener)

/**
   * 设定安装按钮点击事件，默认为渐隐广告
   * @param onClickListener
   * @return
   */
  public SmartAdView listenInstallButtonClick(View.OnClickListener onClickListener)
```

### 更多调用方式 ###

* [Wandoujia SDK Guide](http://developer.wandoujia.com/adnetwork/dev-docs/)